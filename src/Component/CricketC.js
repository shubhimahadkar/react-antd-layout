import React, { Fragment } from 'react'
import { Card, Avatar,Typography } from 'antd'
import Meta from 'antd/lib/card/Meta'
const { Title } = Typography;

export const Cricketer = ({ name, team, avatarScr, children }) =>
    <Card bordered hoverable style={{width:280, float:'left', margin: '5px 5px'}}>
        <Meta
            avatar={<Avatar src={avatarScr} />}
            title={name}
            description={team}
        />
        <hr></hr>
        {children}
    </Card>

export const ODIcareer = ({ matches, children }) =>
    <Card.Grid style={{ width: '100%', margin: '10px 0' }}>
        <Title level={4}>ODI Matches: {matches} </Title>
        {children}
    </Card.Grid>

export const Testcareer = ({ matches, children }) =>
    <Card.Grid style={{ width: '100%', margin: '10px 0'  }}>
        <Title level={4}>Test Matches: {matches} </Title>
        {children}
    </Card.Grid>

export const Batting = ({ runs, score }) =>
    <Fragment>
        Runs : {runs}
        <br />
        Top score : {score}
    </Fragment>

export const Bowling = ({ wickets, bowlingAvg }) =>
    <Fragment>
        Wickets : {wickets}
        <br />
        Bowling Average : {bowlingAvg}
    </Fragment>