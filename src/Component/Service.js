import { useEffect, useState } from 'react'
/**
 * @param {*}url
 */
const useFetchData = (url) => {
    let [isLoading, setIsLoading] = useState(false);
    let [output, setOutput] = useState({});

    useEffect(() => {
        setIsLoading(true);
        async function fetchData() {
            await fetch(url)
                .then(response => {
                    response.json()
                        .then(data => setTimeout(() => {
                            setOutput(data);
                            setIsLoading(false);
                        }, 2000)
                        )
                        .catch(err => {
                            setOutput({});
                            setIsLoading(false);
                        })
                })
        }
        fetchData();
        return () => {
            console.log('current palyer is unmount')
        }
    }, [url])

    return [isLoading, output];
    // return [output];

}

export default useFetchData;