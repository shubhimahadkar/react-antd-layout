import React, { useState, useEffect } from 'react'
import { Drawer, Skeleton } from 'antd'
import useFetchData from './Service';

export const CareerDetails = ({ player, ViewDrawer, onClose }) => {
    // const [Details, setDetails] = useState({});
    const [isLoading, output] =useFetchData('./' + player.replace(' ','_')+'.json')
    // const [output] = useFetchData('./' + player.replace(' ', '_') + '.json')
    const { name, team, age, born, batting, bowling } = output;

    // useEffect(() => {(use multiple useeffect)
    //     effect
    //     return () => {
    //         cleanup
    //     }
    // }, [input])

    // useEffect(() => {
    //     async function fetchData(){
    //         await fetch('./' + player.replace(' ','_')+'.json')
    //         .then(response=>{
    //             response.json()
    //             .then(data=> setDetails(data))
    //         })
    //     }
    //     fetchData();
    //     return () => {
    //         console.log('current palyer is unmount')
    //     }
    // }, [player])

    return (
        <Drawer
            destroyOnClose
            title={player}
            visible={ViewDrawer}
            width={640}
            placement="right"
            onClose={onClose}
        >
            <Skeleton active loading={isLoading} paragraph={{ rows: 4}}>
                <div style={{ padding: 30 }}>
                    <p>Team - {team}</p>
                    <p>Age - {age}</p>
                    <p>Born - {born}</p>
                    <p>Batting - {batting}</p>
                    <p>Bowling - {bowling}</p>

                </div>
            </Skeleton>
        </Drawer>

    )


}