import React, { useState } from 'react';
import { Layout, Typography, Avatar, Menu, Breadcrumb, Button } from 'antd';
import './App.css';
import SubMenu from 'antd/lib/menu/SubMenu';
// import { Bastmen, Bowler } from './Component/Cricketer';
import { Cricketer, ODIcareer, Batting, Bowling, Testcareer } from './Component/CricketC';
import { CareerDetails } from './Component/CareerDetails';


const { Title } = Typography;
const { Header, Footer, Sider, Content } = Layout;

function App() {
  const [selectPalyerName, setSelectPalyerName] = useState('');
  const [ViewDrawer, setViewDrawer] = useState(false)

  const onclose = () => setViewDrawer(false);

  const onselect = name => {
    setSelectPalyerName(name);
    setViewDrawer(true)
  }

  const ViewProfileUser = ({ name }) => {
    return (
      <Button type="dashed" style={{ float: 'right' }} onClick={() => onselect(name)}>
        View full profile ....
      </Button>
    )
  }

  return (
    <div className="App">
      <Layout>
        <Header style={{ padding: 20 }}>
          <Avatar style={{ float: 'right' }}>U</Avatar>
          <Title style={{ color: 'white', textAlign: 'left' }} level={4}>Shubham</Title>
        </Header>
        <Layout>
          <Sider width={300}>
            <Menu
              defaultSelectedKeys={['Dashboard']}
              mode="inline"
              style={{ height: '100%' }}
            >
              <Menu.Item key="Dashboard">Dashboard</Menu.Item>

              <SubMenu
                key="sub1"
                title={
                  <span>
                    <span>Navigation One</span>
                  </span>
                }
              >
                <Menu.ItemGroup key="g1" title="Item 1">
                  <Menu.Item key="1">Option 1</Menu.Item>
                  <Menu.Item key="2">Option 2</Menu.Item>
                </Menu.ItemGroup>
                <Menu.ItemGroup key="g2" title="Item 2">
                  <Menu.Item key="3">Option 3</Menu.Item>
                  <Menu.Item key="4">Option 4</Menu.Item>
                </Menu.ItemGroup>
              </SubMenu>

            </Menu>
          </Sider>

          <Layout>
            <Content
              style={{ padding: '0 50px' }}
            >

              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              </Breadcrumb>

              <div
                style={{
                  padding: 24,
                  marginBottom: '10px',
                  minHeight: 500,
                  backgroundColor: 'white'
                }}
              >
                {/* <Bastmen name='Shubham' team='India' runs='20102' />
                <Bowler name='Omkar' team='India' wickets='100' /> */}

                <Cricketer name='Virak Koli'
                  team='India'
                  avatarScr='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' >

                  <ODIcareer matches='230'>
                    <Batting runs='20,121' score='200' />
                    <br />
                    <Bowling wickets='4' bowlingAvg='166.3' />
                  </ODIcareer>

                  <Testcareer matches='288'>
                    <Batting runs='27,121' score='300' />
                  </Testcareer>

                  <ViewProfileUser name='Virak Koli' />

                </Cricketer>

                <Cricketer name='Vaibhav Mhadgut'
                  team='India'
                  avatarScr='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' >

                  <Testcareer matches='288'>
                    <Batting runs='12,121' score='150' />
                    <br />
                    <Bowling wickets='200' bowlingAvg='166.3' />
                  </Testcareer>

                  <ViewProfileUser name='Vaibhav Mhadgut' />

                </Cricketer>

                <Cricketer name='Omkar Mhadgut'
                  team='India'
                  avatarScr='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' >

                  <ODIcareer matches='50'>
                    <Batting runs='2,121' score='120' />
                  </ODIcareer>

                  <Testcareer matches='150'>
                    <Batting runs='10,121' score='150' />
                    <br />
                    <Bowling wickets='50' bowlingAvg='120.3' />
                  </Testcareer>

                  <ViewProfileUser name='Omkar Mhadgut' />

                </Cricketer>
              </div>

            </Content>

            <CareerDetails player={selectPalyerName} ViewDrawer={ViewDrawer} onClose={onclose} />

            <Footer
              style={{
                textAlign: 'center',
                backgroundColor: '#3399ff',
                minHeight: 5,
                padding: 20
              }}>
              Pran Layout ©2020 example Created by Pran Ltd
            </Footer>
          </Layout>
        </Layout>
      </Layout>

    </div >
  );
}

export default App;
